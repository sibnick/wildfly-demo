package ru.suhorukov.wildfly.demo.service;

import java.util.Random;

public class DemoSrv {

    private static final DemoSrv srv = new DemoSrv();

    private int id;

    private DemoSrv() {
        id = new Random().nextInt();
    }


    public static DemoSrv getInstanse(){
        return srv;
    }

    public int getId(){
        return id;
    }
}
