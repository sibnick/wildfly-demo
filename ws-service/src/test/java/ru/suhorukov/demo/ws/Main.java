package ru.suhorukov.demo.ws;

import ru.suhorukov.wildfly.demo.webapp.NodeEntity;
import ru.suhorukov.wildfly.demo.webapp.ServiceBean;
import ru.suhorukov.wildfly.demo.webapp.ServiceBeanService;

import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class Main{

    public static void main(String[] args) throws Exception {
        Service srv = Service.create(new URL("http://localhost:8080/hibernate/ServiceBean"), ServiceBeanService.SERVICE);
        NodeEntity node = srv.getPort(ServiceBean.class).getNode(2);
        System.out.printf("id=%s, version=%s%n", node.getId(), node.getVersion());
        for(NodeEntity.Tags.Entry e : node.getTags().getEntry()){
            System.out.printf("%s => %s%n", e.getKey(), e.getValue());
        }
    }

}