package ru.suhorukov.wildfly.demo.webapp.util;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.geometric.PGpoint;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

public class PointUserType implements UserType {

    @Override
    public Object assemble(Serializable cached, Object owner) {
        return cached;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object deepCopy(Object o) {
        // It's not a true deep copy, but we store only String instances, and they
        // are immutable, so it should be OK
        if (o == null) {
            return null;
        }
        PGpoint p = (PGpoint)o;
        return new PGpoint(p.x, p.y);
    }

    @Override
    public Serializable disassemble(Object o) {
        return (Serializable) o;
    }

    @Override
    public boolean equals(Object o1, Object o2) {
        return o1==o2 || (o1!=null &&  o1.equals(o2));
    }

    @Override
    public int hashCode(Object o) {
        return o.hashCode();
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) {
        return original;
    }

    @Override
    public Class returnedClass() {
        return Map.class;
    }

    @Override
    public int[] sqlTypes() {
          /*
          * i'm not sure what value should be used here, but it works, AFAIK only
          * length of this array matters, as it is a column span (1 in our case)
          */
        return new int[]{Types.INTEGER};
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object nullSafeGet(ResultSet rs, String[] arg1,
                              SessionImplementor arg2, Object arg3) throws SQLException {
        String col = arg1[0];
        return rs.getObject(col);
    }

    @Override
    public void nullSafeSet(PreparedStatement ps, Object map, int i,
                            SessionImplementor arg3) throws SQLException {
        ps.setObject(i, map);
    }

}
