package ru.suhorukov.wildfly.demo.webapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.suhorukov.wildfly.demo.webapp.entity.NodeEntity;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/search")
@Stateful
public class WebSocketSample {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @PersistenceContext
    private EntityManager em;

    @OnMessage
    public void search(String text, Session client){
        try {
            String obj = MAPPER.writer().writeValueAsString(em.find(NodeEntity.class, Long.parseLong(text)));
            client.getAsyncRemote().sendText(obj);
        }catch (Exception e){
            e.printStackTrace();
            client.getAsyncRemote().sendText("Error: "+e.getMessage());
        }
    }
}
