package ru.suhorukov.wildfly.demo.webapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.suhorukov.wildfly.demo.webapp.entity.NodeEntity;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@Stateless
@Path("/entity")
public class ServiceBean {

    private static final Logger L = LoggerFactory.getLogger(ServiceBean.class);

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void init(){
        L.debug("Em: {}", em);
    }

    @PreDestroy
    public void remove(){
        L.debug("Removed");
    }

    public List<NodeEntity> find(){
        return em.createQuery("select a from NodeEntity a", NodeEntity.class).getResultList();
    }

    @GET
    @Path("{name}/{id}")
    @Produces("application/json")
    public Object get(@PathParam("name") String name, @PathParam("id") Long id){
        L.info("Name; {} / id : {}", name ,id);
        return em.createQuery("from "+name +" o where o.id=:id").setParameter("id", id).getSingleResult();
    }

}
