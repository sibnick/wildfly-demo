package ru.suhorukov.wildfly.demo.webapp;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.suhorukov.wildfly.demo.webapp.entity.NodeEntity;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.*;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/test")
public class FirstServlet extends HttpServlet{

    private static final Logger L = LoggerFactory.getLogger(FirstServlet.class);

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction ut;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EntityManager em = emf.createEntityManager();
        try{
            ut.begin();
            List<NodeEntity> list = em.createQuery("select a from NodeEntity a", NodeEntity.class).getResultList();
            resp.getOutputStream().println("nodes: "+list);
        }catch (Exception e){
            L.error("", e);
        }finally{
            em.close();
            try {
                ut.commit();
            } catch (Exception ex) {
                L.error("", ex);
            }
        }
    }
}
