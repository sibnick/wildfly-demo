package ru.suhorukov.wildfly.demo.webapp.entity;


import org.hibernate.annotations.Type;
import org.postgresql.geometric.PGpoint;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Map;

@Entity
@Table(name = "node")
public class NodeEntity {

    private long id;
    private Integer version;
    private Timestamp cdate;
    private Integer uid;
    private Integer changeset;


    private Map<String, String> tags;
    private PGpoint coord;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "version")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Basic
    @Column(name = "cdate")
    public Timestamp getCdate() {
        return cdate;
    }

    public void setCdate(Timestamp cdate) {
        this.cdate = cdate;
    }

    @Basic
    @Column(name = "uid")
    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "changeset")
    public Integer getChangeset() {
        return changeset;
    }


    public void setChangeset(Integer changeset) {
        this.changeset = changeset;
    }

    @Column(columnDefinition = "hstore", nullable = true)
    @Type(type = "ru.suhorukov.wildfly.demo.webapp.util.HstoreUserType")
    public Map<String, String> getTags() {
        return tags;
    }

    public void setTags(Map<String, String> tags) {
        this.tags = tags;
    }

    @Column(columnDefinition = "point", nullable = true)
    @Type(type = "ru.suhorukov.wildfly.demo.webapp.util.PointUserType")
    public PGpoint getCoord() {
        return coord;
    }

    public void setCoord(PGpoint coord) {
        this.coord = coord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeEntity that = (NodeEntity) o;

        if (id != that.id) return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;
        if (cdate != null ? !cdate.equals(that.cdate) : that.cdate != null) return false;
        if (uid != null ? !uid.equals(that.uid) : that.uid != null) return false;
        if (changeset != null ? !changeset.equals(that.changeset) : that.changeset != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (cdate != null ? cdate.hashCode() : 0);
        result = 31 * result + (uid != null ? uid.hashCode() : 0);
        result = 31 * result + (changeset != null ? changeset.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NodeEntity{" +
                "id=" + id +
                ", version=" + version +
                ", cdate=" + cdate +
                ", uid=" + uid +
                ", changeset=" + changeset +
                ", tags=" + tags +
                ", coord=" + coord +
                '}';
    }
}
