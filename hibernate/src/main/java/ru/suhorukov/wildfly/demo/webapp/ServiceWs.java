package ru.suhorukov.wildfly.demo.webapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.suhorukov.wildfly.demo.webapp.entity.NodeEntity;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@WebService
public class ServiceWs {

    private static final Logger L = LoggerFactory.getLogger(ServiceWs.class);

    @PersistenceContext
    private EntityManager em;

    @WebMethod(exclude = true)
    public List<NodeEntity> find(){
        return em.createQuery("select a from NodeEntity a", NodeEntity.class).getResultList();
    }

    @WebMethod
    public NodeEntity getNode(long id){
        L.info("id : {}" ,id);
        return em.find(NodeEntity.class, id);
    }
}
