package ru.suhorukov.wildfly.demo.webapp;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppInit implements ServletContextListener{

    private static final Logger L = LoggerFactory.getLogger(AppInit.class);


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        L.info("Inited!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        L.info("Destroyed!");
    }
}
