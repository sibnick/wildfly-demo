package ru.suhorukov.wildfly.demo.webapp;

import ru.suhorukov.wildfly.demo.service.DemoSrv;

import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

@WebServlet(urlPatterns = "/test")
public class FirstServlet extends HttpServlet{

    @Resource(lookup = "java:jboss/jdbc/pg-demo")
    private DataSource ds;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(Connection c = ds.getConnection(); Statement tmt = c.createStatement()){
            ResultSet rs = tmt.executeQuery("select 1");
            rs.next();
            System.out.println(rs.getInt(1));
            new InitialContext().lookup("");
        }catch (Exception e){
            e.printStackTrace();
        }
        resp.getOutputStream().println("Hello! "+ DemoSrv.getInstanse().getId());
    }
}
